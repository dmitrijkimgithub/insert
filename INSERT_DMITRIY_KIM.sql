-- Choose one of your favorite films and add it to the "film" table. Fill in rental rates with 4.99 and rental durations with 2 weeks.

INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, length)
VALUES ('SPIDER-MAN. NO WAY HOME', 
'With Spider-Man`s identity now revealed, Peter asks Doctor Strange for help. When a spell goes wrong, dangerous foes from other worlds start to appear, forcing Peter to discover what it truly means to be Spider-Man', 
2021, 1, 14, 4.99, 148);

-- Add the actors who play leading roles in your favorite film to the "actor" and "film_actor" tables (three or more actors in total).

INSERT INTO actor (first_name, last_name)
VALUES ('TOM', 'HOLLAND'), ('ZENDAYA', 'COLEMAN'), ('BENEDICT', 'CUMBERBATCH'), ('ANDREW', 'GARFIELD'), ('TOBEY', 'MAGUIRE');

INSERT INTO film_actor (actor_id, film_id)
VALUES (201, 1001), (202, 1001), (203, 1001), (204, 1001), (205, 1001);

-- Add your favorite movies to any store's inventory.

INSERT INTO inventory (film_id, store_id)
VALUES (1001, 1), (1001, 2), (1001, 1), (1001, 1), (1001, 2);

